#include "marcher.h"  // Includes ImgUtils.h
#include "unittest.h"
#include <assert.h>

Image *IM; 
char *FI = "./images/25colours.ppm";

TEST(urdl) {
	int v = ind(IM,5,5);

	int u = up(IM,v);
	int r = right(IM,v);
	int d = down(IM,v);
	int l = left(IM,v);

	assert(u == ind(IM,5,4));
	assert(r == ind(IM,6,5));
	assert(d == ind(IM,5,6));
	assert(l == ind(IM,4,5));

	int m = -1;
	int i = 1;

	adj(IM,v,v,&m,&i);
	assert(u == m);
	adj(IM,v,v,&m,&i);
	assert(r == m);
	adj(IM,v,v,&m,&i);
	assert(d == m);
	adj(IM,v,v,&m,&i);
	assert(l == m);

	v = ind(IM,0,0);

	u = up(IM,v);
	r = right(IM,v);
	d = down(IM,v);
	l = left(IM,v);

	assert(u == -1);
	assert(r == ind(IM,1,0));
	assert(d == ind(IM,0,1));
	assert(l == -1);

	m = 1;
	i = 1;

	adj(IM,v,v,&m,&i);
	assert(u == m);
	adj(IM,v,v,&m,&i);
	assert(r == m);
	adj(IM,v,v,&m,&i);
	assert(d == m);
	adj(IM,v,v,&m,&i);
	assert(l == m);
}

TEST(ind) {
	int sx = IM->sx;
	int sy = IM->sy;

	assert(ind(IM,sx,sy) == -1);
	assert(ind(IM,sx-5,sy-5) == sx-5+(sy-5)*sy);
	assert(ind(IM,-5,sy-5) == -1);
}

TEST(heap_empty) {
	MinHeap *heap = newMinHeap(1);
	heapPush(heap,0,1.0);
	assert(!heapIsEmpty(heap));
	double pri;
	heapExtractMin(heap,&pri);
	assert(heapIsEmpty(heap));
}

TEST(direction) {
	int ax = 2;
	int ay = 2;
	int a = ind(IM,ax,ay);

	int u = ind(IM,ax,ay-1);
	int r = ind(IM,ax+1,ay);
	int d = ind(IM,ax,ay+1);
	int l = ind(IM,ax-1,ay);

	assert(direction(IM,a,u) == 1);
	assert(direction(IM,a,r) == 2);
	assert(direction(IM,a,d) == 3);
	assert(direction(IM,a,l) == 4);
}

TEST(which_square) {
	//zero+adj
	assert(which_square(IM,ind(IM,20,20)) == 0);
	assert(which_square(IM,ind(IM,41,20)) == 1);
	assert(which_square(IM,ind(IM,20,41)) == 5);
	
	//end
	assert(which_square(IM,ind(IM,160,160)) == 24);
	assert(which_square(IM,ind(IM,160-40,160)) == 23);
	assert(which_square(IM,ind(IM,170,138)) == 19);

	//middle
	assert(which_square(IM,ind(IM,86,96)) == 12);
	assert(which_square(IM,ind(IM,59,91)) == 11);
	assert(which_square(IM,ind(IM,97,138)) == 17);
	assert(which_square(IM,ind(IM,136,93)) == 13);

}

int main(int argc, char *argv[]) {
	Image *im = readPPMimage(FI);
	IM = im;
	unit_main(argc, argv);
	return 0;
}
