/*
 *                        Min-Heaps
 * 
 * This is the Part 1 section of your coding assignment. You will  submit this
 * along with the Part 2 section when it is due, but it is recommended that you
 * complete this early on so you have time to work on second part of this
 * assignment (in `marcher.c` ).
 * 
 * Please make sure you read the blurb in `minheap.h` to ensure you understand 
 * how we are implementing min-heaps here and what assumptions / requirements 
 * are being made.
 * 
 * CSCB63 Winter 2022 - Coding Assignment 2
 * (c) Mustafa Quraish
 */

#include "minheap.h"
#include <string.h>
//used for DBL_MAX
#include <float.h>

const HeapElement MAX = {-1,DBL_MAX};

int is_max(HeapElement v) {
	if (v.priority == MAX.priority && v.val == -1) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * Allocate a new min heap of the given size.
 * 
 * TODO: 
 *  Allocate memory for the `MinHeap` object, and the 2 arrays inside it.
 *  `numItems` should initially be set to 0, and all the indices should be
 *   set to -1 to start with (since we don't have anything in the heap).
 */
MinHeap *newMinHeap(int size) {
	MinHeap *heap = calloc(1,sizeof(MinHeap));
	HeapElement *arr = calloc(size,sizeof(HeapElement));
	int *indices = calloc(size,sizeof(int));

	if (heap == NULL || arr == NULL || indices == NULL ) {
		puts("ENOMEM");
		exit(1);
	}
	for (int i = 0; i < size; i++) {
		indices[i] = -1;
	}

	heap->maxSize = size;
	heap->arr = arr;
	heap->indices = indices;
	return heap;
}

/**
 * Swaps elements at indices `a` and `b` in the heap, and also updates their
 * indices. Assumes that `a` and `b` are valid.
 * 
 * NOTE: This is already implemented for you, no need to change anything.
 */
void swap(MinHeap *heap, int a, int b) {
	// Swap the elements
	HeapElement temp = heap->arr[a];
	heap->arr[a] = heap->arr[b];
	heap->arr[b] = temp;

	// Refresh their indices
	heap->indices[heap->arr[a].val] = a;
	heap->indices[heap->arr[b].val] = b;
}

int get_index(MinHeap *heap, HeapElement v) {
	return heap->indices[v.val];
}

void swaph(MinHeap *heap,HeapElement a, HeapElement b) {
    swap(heap,get_index(heap,a), get_index(heap,b));
    return;
}

HeapElement get_node(MinHeap *heap, int index) {
	return heap->arr[index];
}


/**
 * Add a value with the given priority into the heap.
 * 
 * TODO: Complete this function, and make sure all the relevant data is updated
 *      correctly, including the `indices` array if you move elements around. 
 *      Make sure the heap property is not violated. 
 * 
 * You may assume the value does not already exist in the heap, and there is
 * enough space in the heap for it.
 */
HeapElement heap_parent(MinHeap *heap,HeapElement v) {
	int index = get_index(heap,v);
	if (index == 0) {
		return MAX;
	} else {
                HeapElement p = get_node(heap,(index+1)/2-1);
                if (p.val < 0 && v.val >= 0) {
                    puts("heap_parent generated a negative value");
                    abort();
                }
		return p;
        }
}

int in_bounds(MinHeap *heap,int index)  {
	if (index < heap->numItems) {
		return 1;
	} else {
		return 0;
	}
}

HeapElement get_lchild(MinHeap *heap, HeapElement v) {
	int index = (get_index(heap,v)+1)*2-1;
	if (in_bounds(heap,index)) {
		return get_node(heap,index);
	} else {
		return MAX;
	}
}
HeapElement get_rchild(MinHeap *heap, HeapElement v) {
	int index = (get_index(heap,v)+1)*2+1-1;
	if (in_bounds(heap,index)) {
		return get_node(heap,index);
	} else {
		return MAX;
	}
}

HeapElement newHeapElement(int val,double priority) {
	HeapElement v = {val,priority};
	return v;
}

HeapElement smallest_child(MinHeap *heap,HeapElement v) {
	HeapElement l = get_lchild(heap,v);
	HeapElement r = get_rchild(heap,v);
	if (l.priority < r.priority) {
		return l;
	} else {
		return r;
	}
}


//moves v down
void heapify(MinHeap *heap,HeapElement v) {
	HeapElement schild = smallest_child(heap,v);
	while (!is_max(schild) && schild.priority < v.priority) {
		swaph(heap,v,schild);
		//refresh schild
		schild = smallest_child(heap,v);
	}
	return;
}

//moves v up
void percolate(MinHeap *heap, HeapElement v) {
	HeapElement parent = heap_parent(heap,v);
	while (!is_max(parent) && parent.priority > v.priority) {
		swaph(heap,parent,v);
		parent=heap_parent(heap,v);
	}
}


void heapPush(MinHeap *heap, int val, double priority) {
	if (val >= heap->maxSize || val < 0) {
		puts("invalid heap element pushed");
		return;
	}
	if (heap->indices[val] != -1) {
		puts("U tried to put an index that is already in into the heap, you silly goose!");
		return;
	}
	HeapElement v = newHeapElement(val,priority);
	heap->numItems+=1;
	int numItems = heap->numItems;

	heap->arr[numItems-1] = v;
	heap->indices[val] = numItems-1;
	percolate(heap,get_node(heap,numItems-1));
	return; // Push value to heap before returning
}



/**
 * Extract and return the value from the heap with the minimum priority. Store
 *  the priority for this value in `*priority`. 
 * 
 * For example, if `10` was the value with the lowest priority of `1.0`, then
 *  you would have something that is equivalent to:
 *      
 *        *priority = 1.0;
 *        return 10;
 * 
 * TODO: Complete this function, and make sure all the relevant data is updated
 *      correctly, including the `indices` array if you move elements around. 
 *      Make sure the heap property is not violated. 
 * 
 * You may assume there is at least 1 value in the heap.
 */
int heapExtractMin(MinHeap *heap, double *priority) {
	if (heap->numItems == 0) {
		*priority = 0.0;
		return -1;
	} 

		
	HeapElement min = heap->arr[0];
	*priority = min.priority;
	int val = min.val;

	heap->numItems-=1;
	if (heap->numItems > 0) {
		HeapElement nmin = get_node(heap,heap->numItems);
		swaph(heap,min,nmin);
		heapify(heap,get_node(heap,0));
	} 
	heap->indices[min.val] = -1;
	return val;
}

/**
 * Decrease the priority of the given value (already in the heap) with the
 * new priority.
 * 
 * NOTE: You will find it helpful here to first get the index of the value
 *       in the heap from the `indices` array.
 * 
 * TODO: Complete this function, and make sure all the relevant data is updated
 *      correctly, including the `indices` array if you move elements around. 
 *      Make sure the heap property is not violated. 
 * 
 * You may assume the value is already in the heap, and the new priority is
 *  smaller than the old one (caller is responsible for ensuring this).
 */
void heapDecreasePriority(MinHeap *heap, int val, double priority) {
	if (val < 0 || val >= heap->maxSize) {
		puts("invalid call to heapDecreasePriority");
		return;
	}
	int index = heap->indices[val];
	if (index == -1) {
		puts("value not in heap -- heapDecreasePriority");
		return;
	}
	HeapElement v = get_node(heap,index);

	if (v.priority < priority) {
		puts("Not decreasing priority lol!");
		return;
	}
	v.priority = priority;

	heap->arr[index] = v;

	percolate(heap,v);
	return;   // Decrease priority before return
}

/**
 * Free the data for the heap. This won't be marked, but it is always good
 * practice to free up after yourself when using a language like C.
 */
void freeHeap(MinHeap *heap) {
	free(heap->arr);
	free(heap->indices);
	free(heap);
	return;
}
