CC=clang
CFLAGS=-ggdb -Wall -lm -O0
RFILE=25colours.ppm

all: theap tmarch thelp driver main

driver: marcher.c minheap.c driver.c imgutils.c
	$(CC)  driver.c imgutils.c marcher.c minheap.c -o driver $(CFLAGS)
main: marcher.c minheap.c driver.c imgutils.c main.c
	$(CC)  main.c imgutils.c marcher.c minheap.c -o main $(CFLAGS)
tmarch: test_marcher.c marcher.c minheap.c
	$(CC) imgutils.c test_marcher.c marcher.c minheap.c -o tmarch $(CFLAGS)
thelp: test_help.c marcher.c minheap.c
	$(CC) imgutils.c test_help.c marcher.c minheap.c -o thelp $(CFLAGS)
theap: test_minheap.c minheap.c
	$(CC) test_minheap.c minheap.c -o theap $(CFLAGS)

clean:
	-rm driver tmarch theap thelp Path-* core.* marcher.o f o  valgrind-out.txt main
run: driver
	./driver images/$(RFILE) 3 > f
	nomacs Path-$(RFILE) &
