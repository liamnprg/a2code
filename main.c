#include "minheap.h"
//
#include <stdbool.h>
#include <string.h>
#include <errno.h>

int SIZE = 10;

void printHeap(MinHeap *heap) {
  printf("Heap: (numItems = %d / size = %d)\n", heap->numItems, heap->maxSize);
  printf("  arr = [");
  for (int i=0; i < heap->numItems; i++) {
    printf("(%d, %f), ", heap->arr[i].val, heap->arr[i].priority);
  }
  printf("]\n  ind = [");
  for (int i=0; i < heap->maxSize; i++) {
    printf("%d, ", heap->indices[i]);
  }
  printf("]\n");
}

int get_str(char *buf) {
	char *res = fgets(buf,SIZE,stdin);
	if (res == NULL) {
		return -1;
	} else {
		return 0;
	}
}

int get_value(int *value) {
	char *endptr = NULL;
	int retval;
	errno = -1;


	while (errno != 0) {
		char *buf = (char *) calloc(SIZE+1,'\0');
		errno = 0;
		printf("value: ");
		retval = get_str(buf);
		if (retval == -1) {
			return retval;
		}
		retval = (int) strtol(buf,&endptr,10);
		*value = retval;
		free(buf);
	}
	return 0;
}


int main() {
	char *buf = (char *) calloc(SIZE+1,'\0');
	int value;
	int value2;
	int errmark = 0;

	MinHeap *heap = newMinHeap(100);

	while (errmark != -1) {
		printf("input 2-letter function symbol: ");
		errmark = get_str(buf);
		if (strncmp(buf,"hp",1) == 0) {
			//heapPush
			errmark = get_value(&value);
			if (errmark != -1) {
				errmark = get_value(&value2);
				if (errmark != -1) {
					heapPush(heap,value, (double) value2);
				}
			}
			printf("inserted %d\n",value);
		} else if (strncmp(buf,"em",2) == 0) {
			double pri = -1.0;
			heapExtractMin(heap,&pri);
			printf("Extracted %f\n", pri);
		} else if (strncmp(buf,"dp",2) == 0) {
			//decreasePriority
			errmark = get_value(&value);
			if (errmark != -1) {
				errmark = get_value(&value2);
				if (errmark != -1) {
					heapDecreasePriority(heap,value, (double) value2);
				}
			}
		}
		buf[0] = '\0';
		buf[1] = '\0';
	}
	freeHeap(heap);
	puts("\nbye");
}
